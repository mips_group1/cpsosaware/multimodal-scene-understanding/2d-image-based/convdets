import json
from matplotlib import pyplot as plt

log_dir_name = 'E:/Dropbox/_GroundWork/Automotive/squeezedet-keras/scripts/log_resnet50convdet_v2'
log_dir_name = 'E:/Dropbox/_GroundWork/Automotive/squeezedet-keras/scripts/log_squeezdet'
# myOutFile1
# vc10

datafile_dl='dl_a_3_c_2_accel_10.json'
datafile_vc='vc_accel_10.json'
with open(log_dir_name+'/'+datafile_dl) as dl_json_file:
    with open(log_dir_name+'/'+datafile_vc) as vc_json_file:

        data_vc=json.load(vc_json_file)

        data_dl = json.load(dl_json_file)

        initMap=data_dl[0]['mAP']

        yinit=[initMap for v in data_dl if v['type']=='retrained']
        y_dl = [v['mAP'] for v in data_dl if v['type'] == 'retrained']
        ynoretraining_dl = [v['mAP'] for v in data_dl if v['type'] == 'quantized']

        y_vc = [v['mAP'] for v in data_vc if v['type'] == 'retrained']
        ynoretraining_vc = [v['mAP'] for v in data_vc if v['type'] == 'quantized']

        x = range(len(yinit))

        plt.plot(x, yinit, linestyle='--', marker='o',label='Initial')

        plt.plot(x, y_dl, linestyle='--', marker='o',label='After retraining DL')
        plt.plot(x, y_vc, linestyle='--', marker='o', label='After retraining VC')


        # plt.plot(x, ynoretraining_dl, linestyle='--', marker='o',label='After quantization without retraining DL')
        # plt.plot(x, ynoretraining_vc, linestyle='--', marker='o', label='After quantization without retraining VC')

        plt.xlabel("Accelerated layer")
        plt.ylabel("mAp")
        plt.legend()
        plt.show()







        initVal = data_dl[0]['perClass'][2]['metrics']['precision']

        yinit = [initVal for v in data_dl if v['type'] == 'retrained']
        y_dl = [v['perClass'][2]['metrics']['recall'] for v in data_dl if v['type'] == 'retrained']
        ynoretraining_dl = [v['perClass'][2]['metrics']['precision'] for v in data_dl if v['type'] == 'quantized']

        y_vc = [v['perClass'][2]['metrics']['recall'] for v in data_vc if v['type'] == 'retrained']
        ynoretraining_vc = [v['perClass'][2]['metrics']['precision'] for v in data_vc if v['type'] == 'quantized']

        x = range(len(yinit))

        plt.plot(x, yinit, linestyle='--', marker='o', label='Initial')

        plt.plot(x, y_dl, linestyle='--', marker='o', label='After retraining DL')
        plt.plot(x, y_vc, linestyle='--', marker='o', label='After retraining VC')

        # plt.plot(x, ynoretraining_dl, linestyle='--', marker='o',label='After quantization without retraining DL')
        # plt.plot(x, ynoretraining_vc, linestyle='--', marker='o', label='After quantization without retraining VC')

        plt.xlabel("Accelerated layer")
        plt.ylabel("Car recall")
        plt.legend()
        plt.show()