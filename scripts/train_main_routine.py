import argparse
import os
import gc
import numpy as np
import glob
import os
import tensorflow as tf
# Load networks
from main.model.squeezeDet import SqueezeDet
from main.model.resnet50Det import Resnet50Det
from main.model.resnet50DetBias import Resnet50DetBias
from main.model.resnet50DetV2 import Resnet50DetV2
from main.model.VGG16DetBiasV0 import VGG16DetBiasV0
from main.model.VGG16DetBiasV1 import VGG16DetBiasV1
from main.model.VGG16DetBiasV2 import VGG16DetBiasV2
from main.model.resnet50DetV3 import Resnet50DetV3
from main.model.resnet50DetV4 import Resnet50DetV4
from keras.layers import Layer
from main.model.dataGenerator import generator_from_data_path
from keras.models import load_model
import keras.backend as K
from keras import optimizers
from keras.callbacks import TensorBoard, ModelCheckpoint, LearningRateScheduler, ReduceLROnPlateau, TerminateOnNaN
from keras.callbacks import Callback
from keras.utils import multi_gpu_model
import time
from main.model.modelLoading import load_only_possible_weights, load_only_possible_weights_in_pretrained, \
    load_only_possible_weights_from_tf_resnet50
from main.model.multi_gpu_model_checkpoint import ModelCheckpointMultiGPU
import pickle
from main.config.create_config import load_dict

print(os.getcwd())
root = "E:/Dropbox/_GroundWork/Automotive/squeezedet-keras/"
root_dataset = "E:/Dropbox/_GroundWork/_Datasets/"
os.chdir(root)

img_file = root_dataset + "KITTI/ImageSets/img_train.txt"
gt_file = root_dataset + "KITTI/ImageSets/gt_train.txt"
EPOCHS = 300
STEPS = None
OPTIMIZER = None
CUDA_VISIBLE_DEVICES = "0"
GPUS = 1
PRINT_TIME = 0
REDUCELRONPLATEAU = True
VERBOSE = True
load_partial_weights = False
load_full_network = False
doDelete = False

# squeezedet
# resnet50convdetnoBias
# vgg16convdet
# resnet50convdetv2
# resnet50convdetv3
# resnet50convdetv4
network = 'resnet50convdetv3'

if network == 'vgg16convdet':
    is_trainable = True
    doFromScratch = False
    doTransfer = True
    setModel = VGG16DetBiasV2
    OPTIMIZER = 'adam'
    OPTIMIZER = 'SGD'
    if doTransfer:
        CONFIG = root + "main/config/vgg16convdet.transfer.config"
        log_dir_name = root + "scripts/log_vgg16convdet"
        if doFromScratch:
            init_file = root + "main/model/VGG16.h5"
            load_partial_weights = True
        if not doFromScratch:
            list_of_files = glob.glob(log_dir_name + '/checkpoints/*')  # * means all if need specific format then *.csv
            latest_file = max(list_of_files, key=os.path.getctime)
            print(latest_file)
            init_file = latest_file
            load_full_network = True
    else:
        CONFIG = root + "main/config/vgg16convdet.transfer.config"
        log_dir_name = root + "scripts/log_vgg16convdet"
        init_file = root + "main/model/VGG16.h5"

if network == 'resnet50convdetnoBias':
    is_trainable = not doTransfer
    setModel = Resnet50DetBias
    if doTransfer:
        init_file = root + "main/model/ResNet50v2.h5"
        # init_file=root + "scripts/log_transfer_resnet_nobias/checkpoints/model.01-4.06.hdf5"
        CONFIG = root + "main/config/resnet50convdet.tranfer.config"
        log_dir_name = root + "scripts/log_resnet50convdet_nobias"
        # decay_rate = 0.1
    else:
        CONFIG = root + "main/config/resnet50convdet.config"
        log_dir_name = root + "scripts/log_resnet50convdet_nobias"
        init_file = log_dir_name + "/checkpoints/model.01-0.34.hdf5"

if network == 'resnet50convdetv2':
    doDelete = False
    doTransfer = True
    doFromScratch = False
    load_partial_weights = False
    load_full_network = False

    OPTIMIZER = 'SGD'
    setModel = Resnet50DetV2
    log_dir_name = root + "scripts/log_resnet50convdet_v2"
    if doTransfer:
        CONFIG = root + "main/config/resnet50convdet.config"
        if doFromScratch:
            init_file = root + "main/model/ResNet50v2.h5"
            load_partial_weights = True
            is_trainable = True
        if not doFromScratch:
            list_of_files = glob.glob(log_dir_name + '/checkpoints/*')  # * means all if need specific format then *.csv
            latest_file = max(list_of_files, key=os.path.getctime)
            print(latest_file)
            init_file = latest_file
            load_full_network = True
    else:
        init_file = root + "main/model/ResNet50v2.h5"
        CONFIG = root + "main/config/resnet50convdet.config"

if network == 'resnet50convdetv3':
    doDelete = True
    doTransfer = True
    doFromScratch = True
    OPTIMIZER = 'SGD'
    # OPTIMIZER = 'adam'
    setModel = Resnet50DetV3
    log_dir_name = root + "scripts/log_resnet50convdet_v3"
    if doTransfer:
        CONFIG = root + "main/config/resnet50convdet.tranfer.v3.config"
        if doFromScratch:
            init_file = root + "main/model/ResNet50.h5"
            load_partial_weights = True
            is_trainable = None
            load_from_tf = False
        if not doFromScratch:
            list_of_files = glob.glob(log_dir_name + '/checkpoints/*')  # * means all if need specific format then *.csv
            latest_file = max(list_of_files, key=os.path.getctime)
            print(latest_file)
            init_file = latest_file
            is_trainable = False
            load_full_network = True


    else:
        init_file = root + "main/model/ResNet50v2.h5"
        CONFIG = root + "main/config/resnet50convdet.config"

if network == 'resnet50convdetv4':
    doDelete = False
    doTransfer = True
    doFromScratch = False
    EPOCHS = 100
    STEPS = None
    OPTIMIZER = 'SGD'
    # OPTIMIZER = 'adam'
    setModel = Resnet50DetV4
    log_dir_name = root + "scripts/log_resnet50convdet_v4"

    if doTransfer:
        CONFIG = root + "main/config/resnet50convdet.tranfer.v4.config"
        if doFromScratch:
            init_file = root + "main/model/ResNet50v2.h5"
            tf_init_file = root + "main/pretrained/trainedResNet50Convdet.npy"
            init_file = tf_init_file
            load_partial_weights = True
            load_from_tf = True
            is_trainable = None
        if not doFromScratch:
            list_of_files = glob.glob(log_dir_name + '/checkpoints/*')  # * means all if need specific format then *.csv
            latest_file = max(list_of_files, key=os.path.getctime)
            print(latest_file)
            init_file = latest_file
            load_full_network = True
    else:
        init_file = root + "main/model/ResNet50v2.h5"
        CONFIG = root + "main/config/resnet50convdet.tranfer.config"

if network == 'squeezedet':
    setModel = SqueezeDet
    init_file_ = "E:/Dropbox/_GroundWork/Automotive/squeezedet-keras/main/model/kitti.hdf5"
    print("Weights initialized by name from {}".format(init_file_))


def train():
    checkpoint_dir = log_dir_name + "/checkpoints"
    tb_dir = log_dir_name + "/tensorboard"
    print('Selected network : ' + network)
    if doDelete:
        if tf.gfile.Exists(checkpoint_dir):
            tf.gfile.DeleteRecursively(checkpoint_dir)
        if tf.gfile.Exists(tb_dir):
            tf.gfile.DeleteRecursively(tb_dir)
        tf.gfile.MakeDirs(tb_dir)
        tf.gfile.MakeDirs(checkpoint_dir)
    # open files with images and ground truths files with full path names
    with open(img_file) as imgs:
        img_names = imgs.read().splitlines()
    imgs.close()
    with open(gt_file) as gts:
        gt_names = gts.read().splitlines()
    gts.close()
    cfg = load_dict(CONFIG)
    # add stuff for documentation to config
    cfg.img_file = img_file
    cfg.gt_file = gt_file
    cfg.images = img_names
    cfg.gts = gt_names
    cfg.init_file = init_file
    cfg.EPOCHS = EPOCHS
    cfg.OPTIMIZER = OPTIMIZER
    cfg.CUDA_VISIBLE_DEVICES = CUDA_VISIBLE_DEVICES
    cfg.GPUS = GPUS
    cfg.REDUCELRONPLATEAU = REDUCELRONPLATEAU
    # set gpu
    if GPUS < 2:
        os.environ['CUDA_VISIBLE_DEVICES'] = CUDA_VISIBLE_DEVICES
    else:
        gpus = ""
        for i in range(GPUS):
            gpus += str(i) + ","
        os.environ['CUDA_VISIBLE_DEVICES'] = gpus
    # scale batch size to gpus
    cfg.BATCH_SIZE = cfg.BATCH_SIZE * GPUS
    # compute number of batches per epoch
    nbatches_train, mod = divmod(len(img_names), cfg.BATCH_SIZE)
    # nbatches_train=500
    if STEPS is not None:
        nbatches_train = STEPS
    cfg.STEPS = nbatches_train
    cfg.LR = cfg.LEARNING_RATE * GPUS
    decay_rate = 2 * cfg.LEARNING_RATE / EPOCHS
    if OPTIMIZER == 'SGD':
        opt = optimizers.SGD(lr=cfg.LR, decay=decay_rate, momentum=cfg.MOMENTUM, nesterov=False,
                             clipnorm=cfg.MAX_GRAD_NORM)

    if OPTIMIZER == 'adam':
        opt = optimizers.Adam(lr=cfg.LR, clipnorm=cfg.MAX_GRAD_NORM, amsgrad=True, decay=decay_rate)

    print("Learning rate: {}".format(cfg.LEARNING_RATE * GPUS))
    # print some run info
    print("Number of images: {}".format(len(img_names)))
    print("Number of epochs: {}".format(EPOCHS))
    print("Number of batches: {}".format(nbatches_train))
    print("Batch size: {}".format(cfg.BATCH_SIZE))

    ####################################################################################################################
    config = tf.ConfigProto(allow_soft_placement=True)
    config.gpu_options.allow_growth = True  # dynamically grow the memory used on the GPU
    config.log_device_placement = False  # to log device placement (on which device the operation ran)
    config.gpu_options.per_process_gpu_memory_fraction = 0.95
    sess = tf.Session(config=config)
    K.set_session(sess)
    squeeze = setModel(cfg)
    # squeeze.model.summary()

    ####################################################################################################################

    if load_full_network:
        print('Loading full network data from:')
        print(init_file)

        try:
            squeeze.model.load_weights(init_file)
        # sometimes model loading files, because the file is still locked, so wait a little bit
        except OSError as e:
            print(e)
            time.sleep(10)
            squeeze.model.load_weights(init_file)
    if load_partial_weights:
        print('Loading only available weights from:')
        print(init_file)
        if not load_from_tf:
            load_only_possible_weights(squeeze.model, init_file, verbose=VERBOSE, is_trainable=is_trainable)
        if load_from_tf:
            load_only_possible_weights_from_tf_resnet50(squeeze.model, tf_init_file, verbose=VERBOSE,
                                                        is_trainable=is_trainable)

    ####################################################################################################################

    cb = []
    tnanCallBack = TerminateOnNaN()
    cb.append(tnanCallBack)
    with open(log_dir_name + '/config.pkl', 'wb') as f:
        pickle.dump(cfg, f, pickle.HIGHEST_PROTOCOL)

    # print LR callback
    class LRTracker(Callback):
        def on_epoch_end(self, epoch, logs=None):
            lr = self.model.optimizer.lr
            decay = self.model.optimizer.decay
            iterations = self.model.optimizer.iterations
            lr_with_decay = lr / (1. + decay * K.cast(iterations, K.dtype(decay)))
            print(K.eval(lr_with_decay))

    lrtracker = LRTracker()
    cb.append(lrtracker)
    # add tensorboard callback
    tbCallBack = TensorBoard(log_dir=tb_dir, histogram_freq=0, write_graph=True, write_images=True)
    cb.append(tbCallBack)
    # if flag was given, add reducelronplateu callback
    if REDUCELRONPLATEAU:
        reduce_lr = ReduceLROnPlateau(monitor='loss', factor=0.1, verbose=1, patience=5, min_lr=0.0)
        cb.append(reduce_lr)
    # print keras model summary
    if VERBOSE:
        print(squeeze.model.summary())

    ####################################################################################################################

    # create train generator
    train_generator = generator_from_data_path(img_names, gt_names, config=cfg)
    # make model parallel if specified
    if GPUS > 1:
        # use multigpu model checkpoint
        ckp_saver = ModelCheckpointMultiGPU(checkpoint_dir + "/model.{epoch:02d}-{loss:.2f}.hdf5", monitor='loss',
                                            verbose=0,
                                            save_best_only=False,
                                            save_weights_only=True, mode='auto', period=1)

        cb.append(ckp_saver)
        print("Using multi gpu support with {} GPUs".format(GPUS))

        # make the model parallel
        parallel_model = multi_gpu_model(squeeze.model, gpus=GPUS)
        parallel_model.compile(optimizer=opt,
                               loss=[squeeze.loss],
                               metrics=[squeeze.loss_without_regularization, squeeze.bbox_loss, squeeze.class_loss,
                                        squeeze.conf_loss])

        # actually do the training
        parallel_model.fit_generator(train_generator,
                                     epochs=EPOCHS,
                                     steps_per_epoch=nbatches_train,
                                     # use_multiprocessing=True,
                                     # workers=4,
                                     callbacks=cb)
    else:
        # add a checkpoint saver
        ckp_saver = ModelCheckpoint(checkpoint_dir + "/model.{epoch:02d}-{loss:.2f}.hdf5", monitor='loss', verbose=1,
                                    save_best_only=False,
                                    save_weights_only=True, mode='auto', period=1)
        cb.append(ckp_saver)
        print("Using single GPU")
        # compile model from squeeze object, loss is not a function of model directly
        squeeze.model.compile(optimizer=opt,
                              loss=[squeeze.loss],
                              metrics=[squeeze.loss_without_regularization,
                                       squeeze.bbox_loss,
                                       squeeze.class_loss,
                                       squeeze.conf_loss])
        # actually do the training
        squeeze.model.fit_generator(train_generator,
                                    epochs=EPOCHS,
                                    shuffle=True,
                                    steps_per_epoch=nbatches_train,
                                    callbacks=cb)

    # gc.collect()


if __name__ == "__main__":

    # parse arguments
    parser = argparse.ArgumentParser(description='Train squeezeDet model.')
    parser.add_argument("--steps", type=int, help="steps per epoch. DEFAULT: #imgs/ batch size")
    parser.add_argument("--epochs", type=int, help="number of epochs. DEFAULT: 100")
    parser.add_argument("--optimizer",
                        help="Which optimizer to use. DEFAULT: SGD with Momentum and lr decay OPTIONS: SGD, ADAM")
    parser.add_argument("--logdir", help="dir with checkpoints and loggings. DEFAULT: ./log")
    parser.add_argument("--img", help="file of full path names for the training images. DEFAULT: img_train.txt")
    parser.add_argument("--gt",
                        help="file of full path names for the corresponding training gts. DEFAULT: gt_train.txt")
    parser.add_argument("--gpu", help="which gpu to use. DEFAULT: 0")
    parser.add_argument("--gpus", type=int,
                        help="number of GPUS to use when using multi gpu support. Overwrites gpu flag. DEFAULT: 1")
    parser.add_argument("--init",
                        help="keras checkpoint to start training from. If argument is none, training starts from the beginnin. DEFAULT: init_weights.h5")
    parser.add_argument("--resume", type=bool, help="Resumes training and does not delete old dirs. DEFAULT: False")
    parser.add_argument("--reducelr", type=bool, help="Add ReduceLrOnPlateu callback to training. DEFAULT: True")
    parser.add_argument("--verbose", type=bool, help="Prints additional information. DEFAULT: False")
    parser.add_argument("--config", help="Dictionary of all the hyperparameters. DEFAULT: squeeze.config")

    args = parser.parse_args()

    # set global variables
    if args.img is not None:
        img_file = args.img
    if args.gt is not None:
        gt_file = args.gt
    if args.logdir is not None:
        log_dir_name = args.logdir
    if args.gpu is not None:
        CUDA_VISIBLE_DEVICES = args.gpu
    if args.epochs is not None:
        EPOCHS = args.epochs
    if args.steps is not None:
        STEPS = args.steps
    if args.optimizer is not None:
        OPTIMIZER = 'args.optimizer.lower()'
    if args.init is not None:
        init_file = args.init
    if args.gpus is not None:
        GPUS = args.gpus
    if args.reducelr is not None:
        REDUCELRONPLATEAU = args.reducelr
    if args.verbose is not None:
        VERBOSE = args.verbose
    if args.config is not None:
        CONFIG = args.config

    train()
