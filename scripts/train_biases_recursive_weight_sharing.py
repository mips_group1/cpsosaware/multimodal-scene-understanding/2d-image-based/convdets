# Project: squeezeDetOnKeras
# Filename: train
# Author: Christopher Ehmann
# Date: 08.12.17
# Organisation: searchInk
# Email: christopher@searchink.com


###
import tensorflow.compat.v1 as tf
# tf.disable_v2_behavior()
# tf.compat.v1.disable_eager_execution()

from main.model.squeezeDet import  SqueezeDet
from main.model.resnet50Det import Resnet50Det
from main.model.resnet50DetBias import Resnet50DetBias
from main.model.resnet50DetV2 import Resnet50DetV2


from main.model.VGG16DetBiasV2 import VGG16DetBiasV2


from main.model.dataGenerator import generator_from_data_path
from keras.models import load_model
import keras.backend as K
from keras import optimizers
from keras.callbacks import TensorBoard, ModelCheckpoint, LearningRateScheduler, ReduceLROnPlateau
from keras.utils import multi_gpu_model
import time
from main.model.modelLoading import load_only_possible_weights,load_only_possible_weights_in_pretrained
from main.model.multi_gpu_model_checkpoint import  ModelCheckpointMultiGPU
import argparse
import os
import gc
import numpy as np
import pickle
from main.config.create_config import load_dict
from main.utils.weight_sharing import quant_convlayer_weights
from main.model.evaluation import evaluate
from keras.callbacks import TerminateOnNaN
import warnings
from keras.callbacks import Callback
class EarlyStoppingByLossVal(Callback):
    def __init__(self, monitor='val_loss', value=0.00001, verbose=0):
        super(Callback, self).__init__()
        self.monitor = monitor
        self.value = value
        self.verbose = verbose

    def on_epoch_end(self, epoch, logs={}):
        current = logs.get(self.monitor)
        if current is None:
            warnings.warn("Early stopping requires %s available!" % self.monitor, RuntimeWarning)
        if current < self.value:
            if self.verbose > 0:
                print("Epoch %05d: early stopping THR" % epoch)
            self.model.stop_training = True


def quantize(input):
    #return quant_convlayer_weights(input, sub_dim=8, accel=10, coeff=3, sparsity_level=2, clust_scheme='dl')
    return quant_convlayer_weights(input, sub_dim=8, accel=10, coeff=None,sparsity_level=None,clust_scheme='vq')

#global variables can be set by optional arguments
#TODO: Makes proper variables in train() instead of global arguments.
img_file = "E:/Dropbox/_GroundWork/_Datasets/KITTI/ImageSets/img_train.txt"
gt_file = "E:/Dropbox/_GroundWork/_Datasets/KITTI/ImageSets/gt_train.txt"

img_file_val = "E:/Dropbox/_GroundWork/_Datasets/KITTI/ImageSets/img_val.txt"
gt_file_val = "E:/Dropbox/_GroundWork/_Datasets/KITTI/ImageSets/gt_val.txt"

img_file_test = "E:/Dropbox/_GroundWork/_Datasets/KITTI/ImageSets/img_test.txt"
gt_file_test = "E:/Dropbox/_GroundWork/_Datasets/KITTI/ImageSets/gt_test.txt"

with open(img_file) as imgs:
    img_names = imgs.read().splitlines()
imgs.close()
with open(gt_file) as gts:
    gt_names = gts.read().splitlines()
gts.close()


with open(img_file_val) as imgs:
    img_names_val = imgs.read().splitlines()
imgs.close()
with open(gt_file_val) as gts:
    gt_names_val = gts.read().splitlines()
gts.close()


log_dir_name = 'E:/Dropbox/_GroundWork/Automotive/squeezedet-keras/scripts/log'
EPOCHS = 100
STEPS = None
OPTIMIZER = 'adam'
CUDA_VISIBLE_DEVICES = "0"
GPUS = 1
PRINT_TIME = 0
REDUCELRONPLATEAU = True
VERBOSE=True








# squeezedet
# resnet50convdetbias
# vgg16convdet
# resnet50convdetv2
network='squeezedet'
doDelete=False
reTrainingEpochs=15

reTrainingSteps=None

earlyStoppingCallback=EarlyStoppingByLossVal(monitor='loss_without_regularization', value=0.48, verbose=1)



if network=='resnet50convdetv2':
    CONFIG = "E:/Dropbox/_GroundWork/Automotive/squeezedet-keras/main/config/resnet50convdet.tranfer.config"
    log_dir_name = 'E:/Dropbox/_GroundWork/Automotive/squeezedet-keras/scripts/log_resnet50convdet_v2'
    init_file=log_dir_name+'\checkpoints\model.99-0.49.hdf5'
    setModel=Resnet50DetV2



if network=='vgg16convdet':
    CONFIG = ''
    log_dir_name = ''

if network=='squeezedet':
    CONFIG = 'E:/Dropbox/_GroundWork/Automotive/squeezedet-keras/main/config/squeeze.config'
    log_dir_name = 'E:/Dropbox/_GroundWork/Automotive/squeezedet-keras/scripts/log_squeezdet'
    init_file = log_dir_name + '\checkpoints\model.60-0.94.hdf5'
    setModel=SqueezeDet

if network=='resnet50convdetbias':
    CONFIG = "E:/Dropbox/_GroundWork/Automotive/squeezedet-keras/main/config/resnet50convdet.config"
    log_dir_name = 'E:/Dropbox/_GroundWork/Automotive/squeezedet-keras/scripts/log_resnet50convdet_nobias'
    init_file=log_dir_name+'\checkpoints\model.01-0.36.hdf5'
    setModel=Resnet50DetBias








def train():


    lossesMetric = []
    outF = open(log_dir_name+"/myOutFile.json", "w")

    checkpoint_dir = log_dir_name +"/checkpoints"
    tb_dir = log_dir_name +"/tensorboard"
    #delete old checkpoints and tensorboard stuff
    if doDelete:
        if tf.gfile.Exists(checkpoint_dir):
            tf.gfile.DeleteRecursively(checkpoint_dir)
        if tf.gfile.Exists(tb_dir):
            tf.gfile.DeleteRecursively(tb_dir)
        tf.gfile.MakeDirs(tb_dir)
        tf.gfile.MakeDirs(checkpoint_dir)
    # ============================================================================================================
    #open files with images and ground truths files with full path names
    #create config object
    cfg = load_dict(CONFIG)
    #add stuff for documentation to config
    cfg.img_file = img_file
    cfg.gt_file = gt_file
    cfg.images = img_names
    cfg.gts = gt_names
    cfg.init_file = init_file
    cfg.EPOCHS = EPOCHS
    cfg.OPTIMIZER = OPTIMIZER
    cfg.CUDA_VISIBLE_DEVICES = CUDA_VISIBLE_DEVICES
    cfg.GPUS = GPUS
    cfg.REDUCELRONPLATEAU = REDUCELRONPLATEAU
    #set gpu
    if GPUS < 2:
        os.environ['CUDA_VISIBLE_DEVICES'] = CUDA_VISIBLE_DEVICES
    else:
        gpus = ""
        for i in range(GPUS):
            gpus +=  str(i)+","
        os.environ['CUDA_VISIBLE_DEVICES'] = gpus
    #scale batch size to gpus
    cfg.BATCH_SIZE = cfg.BATCH_SIZE * GPUS
    #compute number of batches per epoch
    nbatches_train, mod = divmod(len(img_names), cfg.BATCH_SIZE)
    if STEPS is not None:
        nbatches_train = STEPS
    cfg.STEPS = nbatches_train
    # ============================================================================================================
    #compute number of batches per epoch
    # nbatches_valid, mod = divmod(len(gt_names), cfg.BATCH_SIZE)
    #print some run info
    print("Number of images: {}".format(len(img_names)))
    print("Number of epochs: {}".format(EPOCHS))
    print("Number of batches: {}".format(nbatches_train))
    print("Batch size: {}".format(cfg.BATCH_SIZE))
    # ============================================================================================================


    # Variables to visualize losses (as metrics) for tensorboard
    loss_var = tf.Variable(
        initial_value=0, trainable=False,
        name='val_loss', dtype=tf.float32
    )

    loss_without_regularization_var = tf.Variable(
        initial_value=0, trainable=False,
        name='val_loss_without_regularization', dtype=tf.float32
    )

    conf_loss_var = tf.Variable(
        initial_value=0, trainable=False,
        name='val_conf_loss', dtype=tf.float32
    )
    class_loss_var = tf.Variable(
        initial_value=0, trainable=False,
        name='val_class_loss', dtype=tf.float32
    )

    bbox_loss_var = tf.Variable(
        initial_value=0, trainable=False,
        name='val_bbox_loss', dtype=tf.float32
    )

    # create placeholders for metrics. Variables get assigned these.
    loss_placeholder = tf.placeholder(loss_var.dtype, shape=())
    loss_without_regularization_placeholder = tf.placeholder(loss_without_regularization_var.dtype, shape=())
    conf_loss_placeholder = tf.placeholder(conf_loss_var.dtype, shape=())
    class_loss_placeholder = tf.placeholder(class_loss_var.dtype, shape=())
    bbox_loss_placeholder = tf.placeholder(bbox_loss_var.dtype, shape=())

    # we have to create the assign ops here and call the assign ops with a feed dict, otherwise memory leak
    loss_assign_ops = [loss_var.assign(loss_placeholder),
                       loss_without_regularization_var.assign(loss_without_regularization_placeholder),
                       conf_loss_var.assign(conf_loss_placeholder),
                       class_loss_var.assign(class_loss_placeholder),
                       bbox_loss_var.assign(bbox_loss_placeholder)]

    tf.summary.scalar("loss", loss_var)
    tf.summary.scalar("loss_without_regularization", loss_without_regularization_var)
    tf.summary.scalar("conf_loss", conf_loss_var)
    tf.summary.scalar("class_loss", class_loss_var)
    tf.summary.scalar("bbox_loss", bbox_loss_var)

    # variables for images to visualize
    images_with_boxes = tf.Variable(
        initial_value=np.zeros((cfg.VISUALIZATION_BATCH_SIZE, cfg.IMAGE_HEIGHT, cfg.IMAGE_WIDTH, 3)), name="image",
        dtype=tf.float32)
    update_placeholder = tf.placeholder(images_with_boxes.dtype, shape=images_with_boxes.get_shape())
    update_images = images_with_boxes.assign(update_placeholder)

    tf.summary.image("images", images_with_boxes, max_outputs=cfg.VISUALIZATION_BATCH_SIZE)

    # variables for precision recall and mean average precision
    precisions = []
    recalls = []
    APs = []
    f1s = []

    # placeholders as above
    precision_placeholders = []
    recall_placeholders = []
    AP_placeholders = []
    f1_placeholders = []
    prmap_assign_ops = []

    # add variables, placeholders and assign ops for each class
    for i, name in enumerate(cfg.CLASS_NAMES):
        print("Creating tensorboard plots for " + name)

        precisions.append(tf.Variable(
            initial_value=0, trainable=False,
            name="precision/" + name, dtype=tf.float32
        ))
        recalls.append(tf.Variable(
            initial_value=0, trainable=False,
            name="recall/" + name, dtype=tf.float32
        ))

        f1s.append(tf.Variable(
            initial_value=0, trainable=False,
            name="f1/" + name, dtype=tf.float32
        ))

        APs.append(tf.Variable(
            initial_value=0, trainable=False,
            name="AP/" + name, dtype=tf.float32
        ))

        precision_placeholders.append(
            tf.placeholder(dtype=precisions[i].dtype,
                           shape=precisions[i].shape))

        recall_placeholders.append(
            tf.placeholder(dtype=recalls[i].dtype,
                           shape=recalls[i].shape))
        AP_placeholders.append(
            tf.placeholder(dtype=APs[i].dtype,
                           shape=APs[i].shape))

        f1_placeholders.append(
            tf.placeholder(dtype=f1s[i].dtype,
                           shape=f1s[i].shape))

        prmap_assign_ops.append(precisions[i].assign(precision_placeholders[i]))
        prmap_assign_ops.append(recalls[i].assign(recall_placeholders[i]))
        prmap_assign_ops.append(APs[i].assign(AP_placeholders[i]))
        prmap_assign_ops.append(f1s[i].assign(f1_placeholders[i]))

    # same for mean average precision

    mAP = tf.Variable(
        initial_value=0, trainable=False,
        name="mAP", dtype=tf.float32
    )

    mAP_placeholder = tf.placeholder(mAP.dtype, shape=())

    prmap_assign_ops.append(mAP.assign(mAP_placeholder))

    tf.summary.scalar("mAP", mAP)

    for i, name in enumerate(cfg.CLASS_NAMES):
        tf.summary.scalar("precision/" + name, precisions[i])
        tf.summary.scalar("recall/" + name, recalls[i])
        tf.summary.scalar("AP/" + name, APs[i])
        tf.summary.scalar("f1/" + name, f1s[i])

    merged = tf.summary.merge_all()


    # ============================================================================================================
    config = tf.ConfigProto(allow_soft_placement=True)
    config.gpu_options.allow_growth = True  # dynamically grow the memory used on the GPU
    config.log_device_placement = False  # to log device placement (on which device the operation ran)
    config.gpu_options.per_process_gpu_memory_fraction = 0.6
    ###
    sess = tf.Session(config=config)
    K.set_session(sess)

    # ============================================================================================================
    decay_rate = cfg.LEARNING_RATE / EPOCHS
    cfg.LR = cfg.LEARNING_RATE * GPUS

    opt = optimizers.SGD(lr=cfg.LEARNING_RATE * GPUS, decay=decay_rate, momentum=cfg.MOMENTUM,
                         nesterov=False, clipnorm=cfg.MAX_GRAD_NORM)


    # opt = optimizers.Adam(lr=cfg.LEARNING_RATE * GPUS,  clipnorm=cfg.MAX_GRAD_NORM)


    print("Learning rate: {}".format(cfg.LEARNING_RATE * GPUS))
    # ============================================================================================================
    # Load model
    squeeze=setModel(cfg)
    # ============================================================================================================
    # Load optimizer
    squeeze.model.compile(optimizer=opt,
                              loss=[squeeze.loss], metrics=[squeeze.bbox_loss, squeeze.class_loss,
                                                            squeeze.conf_loss, squeeze.loss_without_regularization])
    # ============================================================================================================
    # Load weights
    try:
        squeeze.model.load_weights(init_file)
        # sometimes model loading files, because the file is still locked, so wait a little bit
    except OSError as e:
        print(e)
        time.sleep(10)
        squeeze.model.load_weights(init_file)

    print(20 * "=")
    kb = []
    for l in squeeze.model.layers:
        w_b=l.get_weights()
        # if (('res' in l.name) & ('_branch' in l.name)):
        if 'expand3x3' in l.name:
            w_b = l.get_weights()
            lShape=np.shape(w_b[0])
            if len(lShape)>1:
                if (lShape[0],lShape[1])==(3,3):
                    kb.append(l.name)
                    print(l.name)
    print(20 * "=")
    # ============================================================================================================
    #compute number of batches per epoch
    nbatches_valid, mod = divmod(len(gt_names_val), cfg.BATCH_SIZE)
    # ============================================================================================================
    #create train generator
    train_generator = generator_from_data_path(img_names, gt_names, config=cfg)
    val_generator_1 = generator_from_data_path(img_names_val, gt_names_val, config=cfg)
    val_generator_2 = generator_from_data_path(img_names_val, gt_names_val, config=cfg)



    # ============================================================================================================
    line = ""
    print("Compute losses...")
    losses = squeeze.model.evaluate_generator(val_generator_1, steps=nbatches_valid, max_queue_size=10,
                                              use_multiprocessing=False)
    print("Compute losses...")
    sess.run(loss_assign_ops, {loss_placeholder: losses[0],
                               loss_without_regularization_placeholder: losses[4],
                               conf_loss_placeholder: losses[3],
                               class_loss_placeholder: losses[2],
                               bbox_loss_placeholder: losses[1]})
    lossesMetric.append(losses)
    # print losses
    print("  Losses:")
    print(
        "  Loss with regularization: {}   val loss:{} \n     bbox_loss:{} \n     class_loss:{} \n     conf_loss:{}".
            format(losses[0], losses[4], losses[1], losses[2], losses[3]))
    line += "{};{};{};{};{};".format(losses[0], losses[4], losses[1], losses[2], losses[3])
    # compute precision recall and mean average precision
    precision, recall, f1, AP = evaluate(model=squeeze.model, generator=val_generator_2, steps=nbatches_valid,
                                         config=cfg)
    # create feed dict for visualization
    prmap_feed_dict = {}
    for i, name in enumerate(cfg.CLASS_NAMES):
        prmap_feed_dict[precision_placeholders[i]] = precision[i]
        prmap_feed_dict[recall_placeholders[i]] = recall[i]
        prmap_feed_dict[AP_placeholders[i]] = AP[i, 1]
        prmap_feed_dict[f1_placeholders[i]] = f1[i]
        line += "{};{};{};{}".format(precision[i], recall[i], AP[i, 1], f1[i])
    print(cfg.CLASS_NAMES)
    currentmAP = np.mean(AP[:, 1], axis=0)
    prmap_feed_dict[mAP_placeholder] = currentmAP
    print("mAP: {} ".format(currentmAP))
    print(40 * "=")
    # ============================================================================================================
    outF.write( '['
                '{'
                '\"type\":\"initial\",'
                '\"loss_with_regularization\":'+str(losses[0])+','
                '\"loss\":'+str(losses[4])+','
                '\"bbox_loss\":'+str(losses[1])+','
                '\"class_loss\":'+str(losses[2])+','
                '\"conf_loss\":'+str(losses[3])+','
                '\"perClass\":'
                '['
                '{\"class\":\"cyclist\",\"classIndex\":0, \"metrics\":{ \"precision\":'+  str(prmap_feed_dict[precision_placeholders[0]]) +',\"recall\":'+ str(prmap_feed_dict[recall_placeholders[0]]) +',\"AP\":'+ str(prmap_feed_dict[AP_placeholders[0]]) +'}},'
                '{\"class\":\"pedestrian\",\"classIndex\":1, \"metrics\":{ \"precision\":'+ str(prmap_feed_dict[precision_placeholders[1]]) +',\"recall\":'+ str(prmap_feed_dict[recall_placeholders[1]]) +',\"AP\":'+ str(prmap_feed_dict[AP_placeholders[1]]) +'}},'
                '{\"class\":\"car\",\"classIndex\":2, \"metrics\":{ \"precision\":'+str(prmap_feed_dict[precision_placeholders[2]])+',\"recall\":'+ str(prmap_feed_dict[recall_placeholders[2]]) +',\"AP\":'+ str(prmap_feed_dict[AP_placeholders[2]]) +'}}'
                '],'
                '\"mAP\":'+str(currentmAP)+''
                '}'
                )


    #callbacks
    cb = []

    cb.append(earlyStoppingCallback)

    tnanCallBack=TerminateOnNaN()
    cb.append(tnanCallBack)
    with open( log_dir_name  +'/config.pkl', 'wb') as f:
        pickle.dump(cfg, f, pickle.HIGHEST_PROTOCOL)
    #add tensorboard callback
    tbCallBack = TensorBoard(log_dir=tb_dir, histogram_freq=0, write_graph=True, write_images=True)
    cb.append(tbCallBack)
    #if flag was given, add reducelronplateu callback
    if REDUCELRONPLATEAU:
        reduce_lr = ReduceLROnPlateau(monitor='loss', factor=0.1,verbose=1,patience=5, min_lr=0.0)
        cb.append(reduce_lr)

    #print keras model summary
    # if VERBOSE:
    #     print(squeeze.model.summary())

    #make model parallel if specified
    if GPUS > 1:
        #use multigpu model checkpoint
        ckp_saver = ModelCheckpointMultiGPU(checkpoint_dir + "/model.{epoch:02d}-{loss:.2f}.hdf5", monitor='loss', verbose=0,
                                    save_best_only=False,
                                    save_weights_only=True, mode='auto', period=1)
        cb.append(ckp_saver)
        print("Using multi gpu support with {} GPUs".format(GPUS))
        # make the model parallel
        parallel_model = multi_gpu_model(squeeze.model, gpus=GPUS)
        parallel_model.compile(optimizer=opt,
                              loss=[squeeze.loss], metrics=[squeeze.loss_without_regularization, squeeze.bbox_loss, squeeze.class_loss, squeeze.conf_loss])
        #actually do the training
        parallel_model.fit_generator(train_generator, epochs=EPOCHS,
                                        steps_per_epoch=nbatches_train, callbacks=cb)
    else:
        # add a checkpoint saver
        ckp_saver = ModelCheckpoint(checkpoint_dir + "/model_opt.{epoch:02d}-{loss:.2f}.hdf5", monitor='loss', verbose=1,
                                    save_best_only=False,
                                    save_weights_only=True, mode='auto', period=1)
        cb.append(ckp_saver)
        print(20 * "=")
        for idx, lname in enumerate(kb):
            for l in squeeze.model.layers:
                if l.name == lname:
                    print(lname)


                    w_and_b = l.get_weights()
                    # get shape of both weight tensors
                    model_shape = np.array(w_and_b[0].shape)
                    print(model_shape)
                    # w_and_b[0] = quant_convlayer_weights(w_and_b[0], sub_dim=8,accel=20, sparsity_level=2, coeff=2,
                    #                                      clust_scheme='dl')
                    w_and_b[0] = quantize(w_and_b[0])
                    # updade weights
                    l.set_weights(w_and_b)
                    l.trainable = False




                    line = ""
                    print("Compute losses...")
                    losses = squeeze.model.evaluate_generator(val_generator_1, steps=nbatches_valid, max_queue_size=10,
                                                              use_multiprocessing=False)
                    print("Compute losses...")
                    sess.run(loss_assign_ops, {loss_placeholder: losses[0],
                                               loss_without_regularization_placeholder: losses[4],
                                               conf_loss_placeholder: losses[3],
                                               class_loss_placeholder: losses[2],
                                               bbox_loss_placeholder: losses[1]})
                    lossesMetric.append(losses)
                    # print losses
                    print("  Losses:")
                    print(
                        "  Loss with regularization: {}   val loss:{} \n     bbox_loss:{} \n     class_loss:{} \n     conf_loss:{}".
                            format(losses[0], losses[4], losses[1], losses[2], losses[3]))
                    line += "{};{};{};{};{};".format(losses[0], losses[4], losses[1], losses[2], losses[3])
                    # compute precision recall and mean average precision
                    precision, recall, f1, AP = evaluate(model=squeeze.model, generator=val_generator_2,
                                                         steps=nbatches_valid,
                                                         config=cfg)
                    # create feed dict for visualization
                    prmap_feed_dict = {}
                    for i, name in enumerate(cfg.CLASS_NAMES):
                        prmap_feed_dict[precision_placeholders[i]] = precision[i]
                        prmap_feed_dict[recall_placeholders[i]] = recall[i]
                        prmap_feed_dict[AP_placeholders[i]] = AP[i, 1]
                        prmap_feed_dict[f1_placeholders[i]] = f1[i]
                        line += "{};{};{};{}".format(precision[i], recall[i], AP[i, 1], f1[i])
                    currentmAP = np.mean(AP[:, 1], axis=0)
                    prmap_feed_dict[mAP_placeholder] = currentmAP
                    print("mAP: {} ".format(currentmAP))
                    print(40 * "=")
                    outF.write(
                               ',{'
                               '\"type\":\"quantized\",'
                               '\"loss_with_regularization\":' + str(losses[0]) + ','
                                                                                  '\"loss\":' + str(losses[4]) + ','
                                                                                                                 '\"bbox_loss\":' + str(
                        losses[1]) + ','
                                     '\"class_loss\":' + str(losses[2]) + ','
                                                                          '\"conf_loss\":' + str(losses[3]) + ','
                                                                                                              '\"perClass\":'
                                                                                                              '['
                                                                                                              '{\"class\":\"cyclist\",\"classIndex\":0, \"metrics\":{ \"precision\":' + str(
                        prmap_feed_dict[precision_placeholders[0]]) + ',\"recall\":' + str(
                        prmap_feed_dict[recall_placeholders[0]]) + ',\"AP\":' + str(
                        prmap_feed_dict[AP_placeholders[0]]) + '}},'
                                                               '{\"class\":\"pedestrian\",\"classIndex\":1, \"metrics\":{ \"precision\":' + str(
                        prmap_feed_dict[precision_placeholders[1]]) + ',\"recall\":' + str(
                        prmap_feed_dict[recall_placeholders[1]]) + ',\"AP\":' + str(
                        prmap_feed_dict[AP_placeholders[1]]) + '}},'
                                                               '{\"class\":\"car\",\"classIndex\":2, \"metrics\":{ \"precision\":' + str(
                        prmap_feed_dict[precision_placeholders[2]]) + ',\"recall\":' + str(
                        prmap_feed_dict[recall_placeholders[2]]) + ',\"AP\":' + str(
                        prmap_feed_dict[AP_placeholders[2]]) + '}}'
                                                               '],'
                                                               '\"mAP\":' + str(currentmAP) + ''
                                                                                              '}')


                    print("Using single GPU")
                    # compile model from squeeze object, loss is not a function of model directly
                    squeeze.model.compile(optimizer=opt,
                                          loss=[squeeze.loss],
                                          metrics=[squeeze.loss_without_regularization, squeeze.bbox_loss,
                                                   squeeze.class_loss, squeeze.conf_loss])
                    #squeeze.model.summary()
                    # actually do the training





                    reTrainingStepsPerEpoch=500
                    if  reTrainingSteps==None:
                        reTrainingStepsPerEpoch = nbatches_train
                    else:
                        reTrainingStepsPerEpoch = reTrainingSteps
                    print('Retraining steps :' + str(reTrainingStepsPerEpoch))
                    squeeze.model.fit_generator(train_generator, epochs=reTrainingEpochs,callbacks=cb, steps_per_epoch=reTrainingStepsPerEpoch,verbose=True)





                    line = ""
                    print("Compute losses...")
                    losses = squeeze.model.evaluate_generator(val_generator_1, steps=nbatches_valid, max_queue_size=10,
                                                              use_multiprocessing=False)
                    print("Compute losses...")
                    sess.run(loss_assign_ops, {loss_placeholder: losses[0],
                                               loss_without_regularization_placeholder: losses[4],
                                               conf_loss_placeholder: losses[3],
                                               class_loss_placeholder: losses[2],
                                               bbox_loss_placeholder: losses[1]})
                    lossesMetric.append(losses)
                    # print losses
                    print("  Losses:")
                    print(
                        "  Loss with regularization: {}   val loss:{} \n     bbox_loss:{} \n     class_loss:{} \n     conf_loss:{}".
                            format(losses[0], losses[4], losses[1], losses[2], losses[3]))
                    line += "{};{};{};{};{};".format(losses[0], losses[4], losses[1], losses[2], losses[3])
                    # compute precision recall and mean average precision
                    precision, recall, f1, AP = evaluate(model=squeeze.model, generator=val_generator_2,
                                                         steps=nbatches_valid,
                                                         config=cfg)
                    # create feed dict for visualization
                    prmap_feed_dict = {}
                    for i, name in enumerate(cfg.CLASS_NAMES):
                        prmap_feed_dict[precision_placeholders[i]] = precision[i]
                        prmap_feed_dict[recall_placeholders[i]] = recall[i]
                        prmap_feed_dict[AP_placeholders[i]] = AP[i, 1]
                        prmap_feed_dict[f1_placeholders[i]] = f1[i]
                        line += "{};{};{};{}".format(precision[i], recall[i], AP[i, 1], f1[i])
                    currentmAP = np.mean(AP[:, 1], axis=0)
                    prmap_feed_dict[mAP_placeholder] = currentmAP
                    print("mAP: {} ".format(currentmAP))
                    print(40 * "=")
                    outF.write(
                        ',{'
                        '\"type\":\"retrained\",'
                        '\"loss_with_regularization\":' + str(losses[0]) + ','
                                                                           '\"loss\":' + str(losses[4]) + ','
                                                                                                          '\"bbox_loss\":' + str(
                            losses[1]) + ','
                                         '\"class_loss\":' + str(losses[2]) + ','
                                                                              '\"conf_loss\":' + str(losses[3]) + ','
                                                                                                                  '\"perClass\":'
                                                                                                                  '['
                                                                                                                  '{\"class\":\"cyclist\",\"classIndex\":0, \"metrics\":{ \"precision\":' + str(
                            prmap_feed_dict[precision_placeholders[0]]) + ',\"recall\":' + str(
                            prmap_feed_dict[recall_placeholders[0]]) + ',\"AP\":' + str(
                            prmap_feed_dict[AP_placeholders[0]]) + '}},'
                                                                   '{\"class\":\"pedestrian\",\"classIndex\":1, \"metrics\":{ \"precision\":' + str(
                            prmap_feed_dict[precision_placeholders[1]]) + ',\"recall\":' + str(
                            prmap_feed_dict[recall_placeholders[1]]) + ',\"AP\":' + str(
                            prmap_feed_dict[AP_placeholders[1]]) + '}},'
                                                                   '{\"class\":\"car\",\"classIndex\":2, \"metrics\":{ \"precision\":' + str(
                            prmap_feed_dict[precision_placeholders[2]]) + ',\"recall\":' + str(
                            prmap_feed_dict[recall_placeholders[2]]) + ',\"AP\":' + str(
                            prmap_feed_dict[AP_placeholders[2]]) + '}}'
                                                                   '],'
                                                                   '\"mAP\":' + str(currentmAP) + ''
                                                                                                  '}'
                    )

                    # outF.close()

                    # exit(55)

    outF.write(']')
    outF.close()




    # gc.collect()


if __name__ == "__main__":



    #parse arguments
    parser = argparse.ArgumentParser(description='Train squeezeDet model.')
    parser.add_argument("--steps",  type=int, help="steps per epoch. DEFAULT: #imgs/ batch size")
    parser.add_argument("--epochs", type=int, help="number of epochs. DEFAULT: 100")
    parser.add_argument("--optimizer",  help="Which optimizer to use. DEFAULT: SGD with Momentum and lr decay OPTIONS: SGD, ADAM")
    parser.add_argument("--logdir", help="dir with checkpoints and loggings. DEFAULT: ./log")
    parser.add_argument("--img", help="file of full path names for the training images. DEFAULT: img_train.txt")
    parser.add_argument("--gt", help="file of full path names for the corresponding training gts. DEFAULT: gt_train.txt")
    parser.add_argument("--gpu",  help="which gpu to use. DEFAULT: 0")
    parser.add_argument("--gpus", type=int,  help="number of GPUS to use when using multi gpu support. Overwrites gpu flag. DEFAULT: 1")
    parser.add_argument("--init",  help="keras checkpoint to start training from. If argument is none, training starts from the beginnin. DEFAULT: init_weights.h5")
    parser.add_argument("--resume", type=bool, help="Resumes training and does not delete old dirs. DEFAULT: False")
    parser.add_argument("--reducelr", type=bool, help="Add ReduceLrOnPlateu callback to training. DEFAULT: True")
    parser.add_argument("--verbose", type=bool,  help="Prints additional information. DEFAULT: False")
    parser.add_argument("--config",   help="Dictionary of all the hyperparameters. DEFAULT: squeeze.config")

    args = parser.parse_args()


    #set global variables
    if args.img is not None:
        img_file = args.img
    if args.gt is not None:
        gt_file = args.gt
    if args.logdir is not None:
        log_dir_name = args.logdir
    if args.gpu is not None:
        CUDA_VISIBLE_DEVICES = args.gpu
    if args.epochs is not None:
        EPOCHS = args.epochs
    if args.steps is not None:
        STEPS = args.steps
    if args.optimizer is not None:
        OPTIMIZER = 'args.optimizer.lower()'
    if args.init is not None:
        init_file = args.init
    if args.gpus is not None:
        GPUS= args.gpus
    if args.reducelr is not None:
        REDUCELRONPLATEAU = args.reducelr
    if args.verbose is not None:
        VERBOSE=args.verbose
    if args.config is not None:
        CONFIG = args.config

    train()
