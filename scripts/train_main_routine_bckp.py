# Project: squeezeDetOnKeras
# Filename: train
# Author: Christopher Ehmann
# Date: 08.12.17
# Organisation: searchInk
# Email: christopher@searchink.com

import tensorflow as tf

# Load networks
from main.model.squeezeDet import  SqueezeDet
from main.model.resnet50Det import Resnet50Det
from main.model.resnet50DetBias import Resnet50DetBias
from main.model.resnet50DetV2 import Resnet50DetV2
from main.model.VGG16DetBiasV0 import VGG16DetBiasV0
from main.model.VGG16DetBiasV1 import VGG16DetBiasV1
from main.model.VGG16DetBiasV2 import VGG16DetBiasV2
from main.model.resnet50DetV3 import Resnet50DetV3
from main.model.resnet50DetV4 import Resnet50DetV4





from keras.layers import Layer
from main.model.dataGenerator import generator_from_data_path
from keras.models import load_model
import keras.backend as K
from keras import optimizers
from keras.callbacks import TensorBoard, ModelCheckpoint, LearningRateScheduler, ReduceLROnPlateau
from keras.utils import multi_gpu_model
import time
from main.model.modelLoading import load_only_possible_weights,load_only_possible_weights_in_pretrained,load_only_possible_weights_from_tf_resnet50
from main.model.multi_gpu_model_checkpoint import  ModelCheckpointMultiGPU
import argparse
import os
import gc
import numpy as np
import glob
import os

import pickle
from main.config.create_config import load_dict

#global variables can be set by optional arguments
#TODO: Makes proper variables in train() instead of global arguments.
img_file = "E:/Dropbox/_GroundWork/_Datasets/KITTI/ImageSets/img_train.txt"
gt_file = "E:/Dropbox/_GroundWork/_Datasets/KITTI/ImageSets/gt_train.txt"
EPOCHS = 100
STEPS = None
OPTIMIZER = None
CUDA_VISIBLE_DEVICES = "0"
GPUS = 1
PRINT_TIME = 0
REDUCELRONPLATEAU = True
VERBOSE=True
load_partial_weights=False
load_full_network=False
doDelete=False





# squeezedet
# resnet50convdetnoBias
# vgg16convdet
# resnet50convdetv2
# resnet50convdetv3
# resnet50convdetv4
network='resnet50convdetv3'






if network=='vgg16convdet':
    is_trainable = True
    doFromScratch = False
    doTransfer = True
    setModel = VGG16DetBiasV2
    OPTIMIZER = 'adam'
    OPTIMIZER = 'SGD'
    if doTransfer:
        CONFIG = "E:/Dropbox/_GroundWork/Automotive/squeezedet-keras/main/config/vgg16convdet.transfer.config"
        log_dir_name = 'E:/Dropbox/_GroundWork/Automotive/squeezedet-keras/scripts/log_vgg16convdet'
        if doFromScratch:
            init_file = "E:/Dropbox/_GroundWork/Automotive/squeezedet-keras/main/model/VGG16.h5"
            load_partial_weights = True
        if not doFromScratch:
            list_of_files = glob.glob(log_dir_name+'/checkpoints/*')  # * means all if need specific format then *.csv
            latest_file = max(list_of_files, key=os.path.getctime)
            print (latest_file)
            init_file=latest_file
            load_full_network = True
    else:
        CONFIG = "E:/Dropbox/_GroundWork/Automotive/squeezedet-keras/main/config/vgg16convdet.transfer.config"
        log_dir_name = 'E:/Dropbox/_GroundWork/Automotive/squeezedet-keras/scripts/log_vgg16convdet'
        init_file = "E:/Dropbox/_GroundWork/Automotive/squeezedet-keras/main/model/VGG16.h5"






if network=='resnet50convdetnoBias':
    is_trainable=not doTransfer
    setModel = Resnet50DetBias
    if doTransfer:
        init_file= "E:/Dropbox/_GroundWork/Automotive/squeezedet-keras/main/model/ResNet50v2.h5"
        # init_file="E:/Dropbox/_GroundWork/Automotive/squeezedet-keras/scripts/log_transfer_resnet_nobias/checkpoints/model.01-4.06.hdf5"
        CONFIG = "E:/Dropbox/_GroundWork/Automotive/squeezedet-keras/main/config/resnet50convdet.tranfer.config"
        log_dir_name = 'E:/Dropbox/_GroundWork/Automotive/squeezedet-keras/scripts/log_resnet50convdet_nobias'
        # decay_rate = 0.1
    else:
        CONFIG = "E:/Dropbox/_GroundWork/Automotive/squeezedet-keras/main/config/resnet50convdet.config"
        log_dir_name = 'E:/Dropbox/_GroundWork/Automotive/squeezedet-keras/scripts/log_resnet50convdet_nobias'
        init_file= log_dir_name+"/checkpoints/model.01-0.34.hdf5"



if network=='resnet50convdetv2':
    doDelete = False
    load_partial_weights = False
    load_full_network = False
    doFromScratch = False
    doTransfer = True
    OPTIMIZER = 'SGD'
    setModel = Resnet50DetV2
    log_dir_name = 'E:/Dropbox/_GroundWork/Automotive/squeezedet-keras/scripts/log_resnet50convdet_v2'
    if doTransfer:
        CONFIG = "E:/Dropbox/_GroundWork/Automotive/squeezedet-keras/main/config/resnet50convdet.tranfer.config"
        if doFromScratch:
            init_file = "E:/Dropbox/_GroundWork/Automotive/squeezedet-keras/main/model/ResNet50v2.h5"
            load_partial_weights = True
            is_trainable = True
        if not doFromScratch:
            list_of_files = glob.glob(log_dir_name + '/checkpoints/*')  # * means all if need specific format then *.csv
            latest_file = max(list_of_files, key=os.path.getctime)
            print(latest_file)
            init_file = latest_file
            load_full_network = True
    else:
        init_file = "E:/Dropbox/_GroundWork/Automotive/squeezedet-keras/main/model/ResNet50v2.h5"
        CONFIG = "E:/Dropbox/_GroundWork/Automotive/squeezedet-keras/main/config/resnet50convdet.tranfer.config"








if network=='resnet50convdetv3':
    doDelete = False
    doTransfer = True
    doFromScratch = False

    OPTIMIZER = 'SGD'
    # OPTIMIZER = 'adam'
    setModel = Resnet50DetV3
    log_dir_name = 'E:/Dropbox/_GroundWork/Automotive/squeezedet-keras/scripts/log_resnet50convdet_v3'
    if doTransfer:
        CONFIG = "E:/Dropbox/_GroundWork/Automotive/squeezedet-keras/main/config/resnet50convdet.tranfer.v3.config"
        if doFromScratch:
            init_file = "E:/Dropbox/_GroundWork/Automotive/squeezedet-keras/main/model/ResNet50.h5"
            load_partial_weights = True
            is_trainable = None
            load_from_tf = False
        if not doFromScratch:
            list_of_files = glob.glob(log_dir_name + '/checkpoints/*')  # * means all if need specific format then *.csv
            latest_file = max(list_of_files, key=os.path.getctime)
            print(latest_file)
            init_file = latest_file
            is_trainable = False
            load_full_network = True
    else:
        init_file = "E:/Dropbox/_GroundWork/Automotive/squeezedet-keras/main/model/ResNet50v2.h5"
        CONFIG = "E:/Dropbox/_GroundWork/Automotive/squeezedet-keras/main/config/resnet50convdet.tranfer.config"



if network=='resnet50convdetv4':
    doDelete = False
    doTransfer = True
    doFromScratch = False
    EPOCHS = 100
    STEPS = None
    OPTIMIZER = 'SGD'
    #OPTIMIZER = 'adam'
    setModel = Resnet50DetV4
    log_dir_name = 'E:/Dropbox/_GroundWork/Automotive/squeezedet-keras/scripts/log_resnet50convdet_v4'

    if doTransfer:
        CONFIG = "E:/Dropbox/_GroundWork/Automotive/squeezedet-keras/main/config/resnet50convdet.tranfer.v4.config"
        if doFromScratch:
            init_file = "E:/Dropbox/_GroundWork/Automotive/squeezedet-keras/main/model/ResNet50v2.h5"
            tf_init_file="E:/Dropbox/_GroundWork/Automotive/squeezedet-keras/main/pretrained/trainedResNet50Convdet.npy"
            init_file=tf_init_file
            load_partial_weights = True
            load_from_tf = True
            is_trainable = None
        if not doFromScratch:
            list_of_files = glob.glob(log_dir_name + '/checkpoints/*')  # * means all if need specific format then *.csv
            latest_file = max(list_of_files, key=os.path.getctime)
            print(latest_file)
            init_file = latest_file
            load_full_network = True
    else:
        init_file = "E:/Dropbox/_GroundWork/Automotive/squeezedet-keras/main/model/ResNet50v2.h5"
        CONFIG = "E:/Dropbox/_GroundWork/Automotive/squeezedet-keras/main/config/resnet50convdet.tranfer.config"




  # if network=='squeezedet':
    #     squeeze = SqueezeDet(cfg)
    #     init_file_ = "E:/Dropbox/_GroundWork/Automotive/squeezedet-keras/main/model/kitti.hdf5"
    #     print("Weights initialized by name from {}".format(init_file_))
    #     load_only_possible_weights(squeeze.model, init_file_, verbose=VERBOSE,is_trainable=True)


if network=='squeezedet':
    CONFIG = ''
    log_dir_name = ''



def train():
    """Def trains a Keras model of SqueezeDet and stores the checkpoint after each epoch
    """
    #create subdirs for logging of checkpoints and tensorboard stuff
    checkpoint_dir = log_dir_name +"/checkpoints"
    tb_dir = log_dir_name +"/tensorboard"

    print('Selected network : ' + network)

    if doDelete:
        if tf.gfile.Exists(checkpoint_dir):
            tf.gfile.DeleteRecursively(checkpoint_dir)
        if tf.gfile.Exists(tb_dir):
            tf.gfile.DeleteRecursively(tb_dir)
        tf.gfile.MakeDirs(tb_dir)
        tf.gfile.MakeDirs(checkpoint_dir)

    #open files with images and ground truths files with full path names
    with open(img_file) as imgs:
        img_names = imgs.read().splitlines()
    imgs.close()
    with open(gt_file) as gts:
        gt_names = gts.read().splitlines()
    gts.close()
    #create config object
    cfg = load_dict(CONFIG)
    #add stuff for documentation to config
    cfg.img_file = img_file
    cfg.gt_file = gt_file
    cfg.images = img_names
    cfg.gts = gt_names
    cfg.init_file = init_file
    cfg.EPOCHS = EPOCHS
    cfg.OPTIMIZER = OPTIMIZER
    cfg.CUDA_VISIBLE_DEVICES = CUDA_VISIBLE_DEVICES
    cfg.GPUS = GPUS
    cfg.REDUCELRONPLATEAU = REDUCELRONPLATEAU


    #set gpu
    if GPUS < 2:
        os.environ['CUDA_VISIBLE_DEVICES'] = CUDA_VISIBLE_DEVICES
    else:
        gpus = ""
        for i in range(GPUS):
            gpus +=  str(i)+","
        os.environ['CUDA_VISIBLE_DEVICES'] = gpus
    #scale batch size to gpus
    cfg.BATCH_SIZE = cfg.BATCH_SIZE * GPUS
    #compute number of batches per epoch
    nbatches_train, mod = divmod(len(img_names), cfg.BATCH_SIZE)

    # nbatches_train=500

    if STEPS is not None:
        nbatches_train = STEPS
    cfg.STEPS = nbatches_train
    cfg.LR = cfg.LEARNING_RATE * GPUS



    decay_rate = cfg.LEARNING_RATE / EPOCHS
    # if OPTIMIZER == "rmsprop":
    #     opt = optimizers.RMSprop(lr=0.001 * GPUS,  clipnorm=cfg.MAX_GRAD_NORM)
    #     cfg.LR= 0.001 * GPUS
    #
    # if OPTIMIZER == "adagrad":
    #     opt = optimizers.Adagrad(lr=0.5 * GPUS,  clipnorm=cfg.MAX_GRAD_NORM)
    #     cfg.LR = 1 * GPUS
    if OPTIMIZER=='SGD':
        opt = optimizers.SGD(lr=cfg.LR, decay=decay_rate, momentum=cfg.MOMENTUM,nesterov=False, clipnorm=cfg.MAX_GRAD_NORM)

    if OPTIMIZER == 'adam':
        opt = optimizers.Adam(lr=cfg.LR, clipnorm=cfg.MAX_GRAD_NORM,amsgrad=True,decay=decay_rate)

    print("Learning rate: {}".format(cfg.LEARNING_RATE * GPUS))






    #print some run info
    print("Number of images: {}".format(len(img_names)))
    print("Number of epochs: {}".format(EPOCHS))
    print("Number of batches: {}".format(nbatches_train))
    print("Batch size: {}".format(cfg.BATCH_SIZE))


    config = tf.ConfigProto(allow_soft_placement=True)
    config.gpu_options.allow_growth = True  # dynamically grow the memory used on the GPU
    config.log_device_placement = False  # to log device placement (on which device the operation ran)
    config.gpu_options.per_process_gpu_memory_fraction=0.66

    sess = tf.Session(config=config)
    K.set_session(sess)
    squeeze = setModel(cfg)
    # squeeze.model.summary()
    # exit(4)
    if load_full_network:
        print('Loading full network data from:')
        print(init_file)
        try:
            squeeze.model.load_weights(init_file)
        # sometimes model loading files, because the file is still locked, so wait a little bit
        except OSError as e:
            print(e)
            time.sleep(10)
            squeeze.model.load_weights(init_file)
    if load_partial_weights:
        print('Loading only available weights from:')
        print(init_file)
        if not load_from_tf:
            load_only_possible_weights(squeeze.model, init_file, verbose=VERBOSE,is_trainable=is_trainable)
        if load_from_tf:
            load_only_possible_weights_from_tf_resnet50(squeeze.model, tf_init_file, verbose=VERBOSE,is_trainable=is_trainable)





    #callbacks
    cb = []


    #set optimizer============================================================================
    #multiply by number of workers do adjust for increased batch size
    # if OPTIMIZER == "adam":
    #     opt = optimizers.Adam(lr=0.001 * GPUS,  clipnorm=cfg.MAX_GRAD_NORM,)
    #     cfg.LR= 0.001 * GPUS

    # else:
    #     # create sgd with momentum and gradient clipping
    #     opt = optimizers.SGD(lr=cfg.LEARNING_RATE * GPUS, decay=0, momentum=cfg.MOMENTUM,
    #                          nesterov=False, clipnorm=cfg.MAX_GRAD_NORM)
    #     cfg.LR = cfg.LEARNING_RATE  * GPUS
    #     print("Learning rate: {}".format(cfg.LEARNING_RATE * GPUS))
    #     # add manuall learning rate decay
    #     #lrCallback = LearningRateScheduler(schedule)
    #     #cb.append(lrCallback)
    # #save config file to log dir









    with open( log_dir_name  +'/config.pkl', 'wb') as f:
        pickle.dump(cfg, f, pickle.HIGHEST_PROTOCOL)

    #add tensorboard callback
    tbCallBack = TensorBoard(log_dir=tb_dir, histogram_freq=0, write_graph=True, write_images=True)
    cb.append(tbCallBack)
    #if flag was given, add reducelronplateu callback
    if REDUCELRONPLATEAU:
        reduce_lr = ReduceLROnPlateau(monitor='loss', factor=0.1,verbose=1,patience=5, min_lr=0.0)
        cb.append(reduce_lr)

    #print keras model summary
    # if VERBOSE:
    #     print(squeeze.model.summary())
    # exit(33)



    # weights = squeeze.model.get_weights()
    # # weights = [np.random.permutation(w.flat).reshape(w.shape) for w in weights]
    # weights = [np.random.normal(0, 0.1, len(w.flat)).reshape(w.shape) for w in weights]
    # # Faster, but less random: only permutes along the first dimension
    # # weights = [np.random.permutation(w) for w in weights]
    # squeeze.model.set_weights(weights)

    # init_file_ = "E:/Dropbox/_GroundWork/Automotive/squeezedet-keras/main/model/ResNet50.h5"
    # print("Weights initialized by name from {}".format(init_file_))
    # load_only_possible_weights(squeeze.model, init_file_, verbose=VERBOSE)
    #
    # init_file_ = "E:/Dropbox/_GroundWork/Automotive/squeezedet-keras/main/model/kitti.hdf5"
    # print("Weights initialized by name from {}".format(init_file_))
    # load_only_possible_weights(squeeze.model, init_file_, verbose=VERBOSE)

    # if init_file != "none":
    #     print("Weights initialized by name from {}".format(init_file))
    #     load_only_possible_weights(squeeze.model, init_file, verbose=VERBOSE)


    # since these layers already existed in the ckpt they got loaded, you can reinitialized them. TODO set flag for that
    # for layer in squeeze.model.layers:
    #     for v in layer.__dict__:
    #         v_arg = getattr(layer, v)
    #         if "fire10" in layer.name or "fire11" in layer.name or "conv12" in layer.name:
    #             if hasattr(v_arg, 'initializer'):
    #                 initializer_method = getattr(v_arg, 'initializer')
    #                 initializer_method.run(session=sess)
    #                 #print('reinitializing layer {}.{}'.format(layer.name, v))










    #create train generator
    train_generator = generator_from_data_path(img_names, gt_names, config=cfg)

    #make model parallel if specified
    if GPUS > 1:

        #use multigpu model checkpoint
        ckp_saver = ModelCheckpointMultiGPU(checkpoint_dir + "/model.{epoch:02d}-{loss:.2f}.hdf5", monitor='loss', verbose=0,
                                    save_best_only=False,
                                    save_weights_only=True, mode='auto', period=1)

        cb.append(ckp_saver)
        print("Using multi gpu support with {} GPUs".format(GPUS))

        # make the model parallel
        parallel_model = multi_gpu_model(squeeze.model, gpus=GPUS)
        parallel_model.compile(optimizer=opt,
                              loss=[squeeze.loss], metrics=[squeeze.loss_without_regularization, squeeze.bbox_loss, squeeze.class_loss, squeeze.conf_loss])



        #actually do the training
        parallel_model.fit_generator(train_generator,
                                     epochs=EPOCHS,
                                     steps_per_epoch=nbatches_train,
                                     # use_multiprocessing=True,
                                     # workers=4,
                                     callbacks=cb)


    else:

        # add a checkpoint saver
        ckp_saver = ModelCheckpoint(checkpoint_dir + "/model.{epoch:02d}-{loss:.2f}.hdf5", monitor='loss', verbose=1,
                                    save_best_only=False,
                                    save_weights_only=True, mode='auto', period=1)
        cb.append(ckp_saver)


        print("Using single GPU")
        #compile model from squeeze object, loss is not a function of model directly
        squeeze.model.compile(optimizer=opt,
                              loss=[squeeze.loss], metrics=[squeeze.loss_without_regularization, squeeze.bbox_loss, squeeze.class_loss, squeeze.conf_loss])

        #actually do the training
        squeeze.model.fit_generator(train_generator, epochs=EPOCHS,shuffle=True,
                                        steps_per_epoch=nbatches_train, callbacks=cb)


    # gc.collect()


if __name__ == "__main__":



    #parse arguments
    parser = argparse.ArgumentParser(description='Train squeezeDet model.')
    parser.add_argument("--steps",  type=int, help="steps per epoch. DEFAULT: #imgs/ batch size")
    parser.add_argument("--epochs", type=int, help="number of epochs. DEFAULT: 100")
    parser.add_argument("--optimizer",  help="Which optimizer to use. DEFAULT: SGD with Momentum and lr decay OPTIONS: SGD, ADAM")
    parser.add_argument("--logdir", help="dir with checkpoints and loggings. DEFAULT: ./log")
    parser.add_argument("--img", help="file of full path names for the training images. DEFAULT: img_train.txt")
    parser.add_argument("--gt", help="file of full path names for the corresponding training gts. DEFAULT: gt_train.txt")
    parser.add_argument("--gpu",  help="which gpu to use. DEFAULT: 0")
    parser.add_argument("--gpus", type=int,  help="number of GPUS to use when using multi gpu support. Overwrites gpu flag. DEFAULT: 1")
    parser.add_argument("--init",  help="keras checkpoint to start training from. If argument is none, training starts from the beginnin. DEFAULT: init_weights.h5")
    parser.add_argument("--resume", type=bool, help="Resumes training and does not delete old dirs. DEFAULT: False")
    parser.add_argument("--reducelr", type=bool, help="Add ReduceLrOnPlateu callback to training. DEFAULT: True")
    parser.add_argument("--verbose", type=bool,  help="Prints additional information. DEFAULT: False")
    parser.add_argument("--config",   help="Dictionary of all the hyperparameters. DEFAULT: squeeze.config")

    args = parser.parse_args()


    #set global variables
    if args.img is not None:
        img_file = args.img
    if args.gt is not None:
        gt_file = args.gt
    if args.logdir is not None:
        log_dir_name = args.logdir
    if args.gpu is not None:
        CUDA_VISIBLE_DEVICES = args.gpu
    if args.epochs is not None:
        EPOCHS = args.epochs
    if args.steps is not None:
        STEPS = args.steps
    if args.optimizer is not None:
        OPTIMIZER = 'args.optimizer.lower()'
    if args.init is not None:
        init_file = args.init
    if args.gpus is not None:
        GPUS= args.gpus
    if args.reducelr is not None:
        REDUCELRONPLATEAU = args.reducelr
    if args.verbose is not None:
        VERBOSE=args.verbose
    if args.config is not None:
        CONFIG = args.config

    train()
