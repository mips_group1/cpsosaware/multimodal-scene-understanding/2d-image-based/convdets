# Project: squeezeDetOnKeras
# Filename: train
# Author: Christopher Ehmann
# Date: 08.12.17
# Organisation: searchInk
# Email: christopher@searchink.com

import tensorflow as tf
from main.model.squeezeDet import  SqueezeDet
from main.model.resnet50Det import Resnet50Det
from main.model.dataGenerator import generator_from_data_path
import keras.backend as K
from keras import optimizers
import tensorflow as tf
from keras.callbacks import TensorBoard, ModelCheckpoint, LearningRateScheduler, ReduceLROnPlateau
from main.model.modelLoading import load_only_possible_weights,load_only_possible_weights_in_pretrained
from main.model.multi_gpu_model_checkpoint import  ModelCheckpointMultiGPU
import argparse
import os
import gc
import numpy as np
from keras.utils import multi_gpu_model
import pickle
from main.config.create_config import load_dict
from main.utils.weight_sharing import quant_convlayer_weights

#global variables can be set by optional arguments
#TODO: Makes proper variables in train() instead of global arguments.
img_file = "E:/Dropbox/_GroundWork/_Datasets/KITTI/ImageSets/img_train.txt"
gt_file = "E:/Dropbox/_GroundWork/_Datasets/KITTI/ImageSets/gt_train.txt"
log_dir_name = 'E:/Dropbox/_GroundWork/Automotive/squeezedet-keras/scripts/log'
init_file = "E:/Dropbox/_GroundWork/Automotive/squeezedet-keras/main/model/squeezenet.h5"
init_file = "E:/Dropbox/_GroundWork/Automotive/squeezedet-keras/main/model/resnet50_weights_tf_dim_ordering_tf_kernels.h5"
init_file = "E:/Dropbox/_GroundWork/Automotive/squeezedet-keras/main/model/resnet50_weights_tf_dim_ordering_tf_kernels_notop.h5"
init_file = "E:/Dropbox/_GroundWork/Automotive/squeezedet-keras/main/model/imagenet.h5"
init_file ="none"
init_file = "E:/Dropbox/_GroundWork/Automotive/squeezedet-keras/main/model/ResNet50.h5"
init_file = "E:/Dropbox/_GroundWork/Automotive/squeezedet-keras/main/model/kitti.hdf5"
EPOCHS = 100
STEPS = None
OPTIMIZER = 'adam'
CUDA_VISIBLE_DEVICES = "0"
GPUS = 1
PRINT_TIME = 0
REDUCELRONPLATEAU = True
VERBOSE=True
network='resnet50convdet'






if network=='squeezedet':
    CONFIG = "E:/Dropbox/_GroundWork/Automotive/squeezedet-keras/main/config/squeeze.config"
    log_dir_name = 'E:/Dropbox/_GroundWork/Automotive/squeezedet-keras/scripts/log'


if network=='resnet50convdet':
    CONFIG = "E:/Dropbox/_GroundWork/Automotive/squeezedet-keras/main/config/resnet50convdet.config"
    log_dir_name = 'E:/Dropbox/_GroundWork/Automotive/squeezedet-keras/scripts/log_tweaks'


def train():
    """Def trains a Keras model of SqueezeDet and stores the checkpoint after each epoch
    """
    #create subdirs for logging of checkpoints and tensorboard stuff
    checkpoint_dir = log_dir_name +"/checkpoints"
    tb_dir = log_dir_name +"/tensorboard"
    #delete old checkpoints and tensorboard stuff
    # if tf.gfile.Exists(checkpoint_dir):
    #     tf.gfile.DeleteRecursively(checkpoint_dir)
    # if tf.gfile.Exists(tb_dir):
    #     tf.gfile.DeleteRecursively(tb_dir)
    # tf.gfile.MakeDirs(tb_dir)
    # tf.gfile.MakeDirs(checkpoint_dir)

    #open files with images and ground truths files with full path names
    with open(img_file) as imgs:
        img_names = imgs.read().splitlines()
    imgs.close()
    with open(gt_file) as gts:
        gt_names = gts.read().splitlines()
    gts.close()
    #create config object
    cfg = load_dict(CONFIG)
    #add stuff for documentation to config
    cfg.img_file = img_file
    cfg.gt_file = gt_file
    cfg.images = img_names
    cfg.gts = gt_names
    cfg.init_file = init_file
    cfg.EPOCHS = EPOCHS
    cfg.OPTIMIZER = OPTIMIZER
    cfg.CUDA_VISIBLE_DEVICES = CUDA_VISIBLE_DEVICES
    cfg.GPUS = GPUS
    cfg.REDUCELRONPLATEAU = REDUCELRONPLATEAU
    #set gpu
    if GPUS < 2:

        os.environ['CUDA_VISIBLE_DEVICES'] = CUDA_VISIBLE_DEVICES

    else:

        gpus = ""
        for i in range(GPUS):
            gpus +=  str(i)+","
        os.environ['CUDA_VISIBLE_DEVICES'] = gpus


    #scale batch size to gpus
    cfg.BATCH_SIZE = cfg.BATCH_SIZE * GPUS
    #compute number of batches per epoch
    nbatches_train, mod = divmod(len(img_names), cfg.BATCH_SIZE)
    if STEPS is not None:
        nbatches_train = STEPS

    cfg.STEPS = nbatches_train

    #print some run info
    print("Number of images: {}".format(len(img_names)))
    print("Number of epochs: {}".format(EPOCHS))
    print("Number of batches: {}".format(nbatches_train))
    print("Batch size: {}".format(cfg.BATCH_SIZE))


    config = tf.ConfigProto(allow_soft_placement=True)
    config.gpu_options.allow_growth = True  # dynamically grow the memory used on the GPU
    config.log_device_placement = False  # to log device placement (on which device the operation ran)
    config.gpu_options.per_process_gpu_memory_fraction=0.6

    sess = tf.Session(config=config)
    K.set_session(sess)
    #instantiate model

    #TODO correct config input in resnet

    if network == 'resnet50convdet':
        squeeze=Resnet50Det(cfg)
        init_file_reference = "E:/Dropbox/_GroundWork/Automotive/squeezedet-keras/main/model/ResNet50v2.h5"
        # init_file_target="E:\Dropbox\_GroundWork\Automotive\squeezedet-keras\scripts\log_resnet50convdet\checkpoints\model.26-7.95.hdf5"
        # init_file_target="E:\Dropbox\_GroundWork\Automotive\squeezedet-keras\scripts\log_resnet50convdet\checkpoints\model.20-7.32.hdf5"
        # init_file_target="E:\Dropbox\_GroundWork\Automotive\squeezedet-keras\scripts\log_resnet50convdet\checkpoints\model.06-3.07.hdf5"

        init_file_target="E:\Dropbox\_GroundWork\Automotive\squeezedet-keras\scripts\log_resnet50convdet\checkpoints\model.61-0.50.hdf5"

        print("Weights initialized by name from {}".format(init_file_target))
        # load_only_possible_weights_in_pretrained(squeeze.model, init_file_target,init_file_reference, verbose=VERBOSE,is_trainable=False)
        # load_only_possible_weights(squeeze.model, init_file_target, verbose=VERBOSE,is_trainable=False)
        squeeze.model.load_weights(init_file_target)

    if network=='squeezedet':
        squeeze = SqueezeDet(cfg)
        init_file_ = "E:/Dropbox/_GroundWork/Automotive/squeezedet-keras/main/model/kitti.hdf5"
        print("Weights initialized by name from {}".format(init_file_))
        load_only_possible_weights(squeeze.model, init_file_, verbose=VERBOSE,is_trainable=True)

        # iterate layers

    print(20 * "=")
    kb = []
    for l in squeeze.model.layers:
        # get the current models weights
        # w_and_b = l.get_weights()
        if ('res' in l.name) & ('_branch' in l.name):
            kb.append(l.name)
            print(l.name)

    print(20 * "=")
    for idx, lname in enumerate(kb[0]):
        for l in squeeze.model.layers:
            if l.name == lname:
                print(lname)
                w_and_b = l.get_weights()
                # get shape of both weight tensors
                model_shape = np.array(w_and_b[0].shape)
                print(model_shape)
                # w_and_b[0] = quant_convlayer_weights(w_and_b[0], sub_dim=8,accel=20, sparsity_level=2, coeff=2,
                #                                      clust_scheme='dl')
                w_and_b[0] = quant_convlayer_weights(w_and_b[0], sub_dim=8, accel=20, coeff=None, sparsity_level=None,
                                                     clust_scheme='vq')
                # updade weights
                l.set_weights(w_and_b)
                l.trainable=False















    #callbacks
    cb = []
    #set optimizer============================================================================
    #multiply by number of workers do adjust for increased batch size
    # if OPTIMIZER == "adam":
    #     opt = optimizers.Adam(lr=0.001 * GPUS,  clipnorm=cfg.MAX_GRAD_NORM,)
    #     cfg.LR= 0.001 * GPUS
    # if OPTIMIZER == "rmsprop":
    #     opt = optimizers.RMSprop(lr=0.001 * GPUS,  clipnorm=cfg.MAX_GRAD_NORM)
    #     cfg.LR= 0.001 * GPUS
    #
    # if OPTIMIZER == "adagrad":
    #     opt = optimizers.Adagrad(lr=0.5 * GPUS,  clipnorm=cfg.MAX_GRAD_NORM)
    #     cfg.LR = 1 * GPUS
    # else:
    #     # create sgd with momentum and gradient clipping
    #     opt = optimizers.SGD(lr=cfg.LEARNING_RATE * GPUS, decay=0, momentum=cfg.MOMENTUM,
    #                          nesterov=False, clipnorm=cfg.MAX_GRAD_NORM)
    #     cfg.LR = cfg.LEARNING_RATE  * GPUS
    #     print("Learning rate: {}".format(cfg.LEARNING_RATE * GPUS))
    #     # add manuall learning rate decay
    #     #lrCallback = LearningRateScheduler(schedule)
    #     #cb.append(lrCallback)
    # #save config file to log dir




    decay_rate = cfg.LEARNING_RATE / EPOCHS
    #decay_rate =0
    opt = optimizers.SGD(lr=cfg.LEARNING_RATE * GPUS, decay=decay_rate, momentum=cfg.MOMENTUM,
                         nesterov=False, clipnorm=cfg.MAX_GRAD_NORM)
    cfg.LR = cfg.LEARNING_RATE * GPUS

    # opt = optimizers.Adam(lr=cfg.LEARNING_RATE, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=decay_rate)
    # cfg.LR= 0.001 * GPUS
    print("Learning rate: {}".format(cfg.LEARNING_RATE * GPUS))




    with open( log_dir_name  +'/config.pkl', 'wb') as f:
        pickle.dump(cfg, f, pickle.HIGHEST_PROTOCOL)

    #add tensorboard callback
    tbCallBack = TensorBoard(log_dir=tb_dir, histogram_freq=0, write_graph=True, write_images=True)
    cb.append(tbCallBack)
    #if flag was given, add reducelronplateu callback
    if REDUCELRONPLATEAU:
        reduce_lr = ReduceLROnPlateau(monitor='loss', factor=0.1,verbose=1,patience=5, min_lr=0.0)
        cb.append(reduce_lr)

    #print keras model summary
    if VERBOSE:
        print(squeeze.model.summary())



    # weights = squeeze.model.get_weights()
    # # weights = [np.random.permutation(w.flat).reshape(w.shape) for w in weights]
    # weights = [np.random.normal(0, 0.1, len(w.flat)).reshape(w.shape) for w in weights]
    # # Faster, but less random: only permutes along the first dimension
    # # weights = [np.random.permutation(w) for w in weights]
    # squeeze.model.set_weights(weights)

    # init_file_ = "E:/Dropbox/_GroundWork/Automotive/squeezedet-keras/main/model/ResNet50.h5"
    # print("Weights initialized by name from {}".format(init_file_))
    # load_only_possible_weights(squeeze.model, init_file_, verbose=VERBOSE)
    #
    # init_file_ = "E:/Dropbox/_GroundWork/Automotive/squeezedet-keras/main/model/kitti.hdf5"
    # print("Weights initialized by name from {}".format(init_file_))
    # load_only_possible_weights(squeeze.model, init_file_, verbose=VERBOSE)

    # if init_file != "none":
    #     print("Weights initialized by name from {}".format(init_file))
    #     load_only_possible_weights(squeeze.model, init_file, verbose=VERBOSE)


    # since these layers already existed in the ckpt they got loaded, you can reinitialized them. TODO set flag for that
    # for layer in squeeze.model.layers:
    #     for v in layer.__dict__:
    #         v_arg = getattr(layer, v)
    #         if "fire10" in layer.name or "fire11" in layer.name or "conv12" in layer.name:
    #             if hasattr(v_arg, 'initializer'):
    #                 initializer_method = getattr(v_arg, 'initializer')
    #                 initializer_method.run(session=sess)
    #                 #print('reinitializing layer {}.{}'.format(layer.name, v))






    #create train generator
    train_generator = generator_from_data_path(img_names, gt_names, config=cfg)

    #make model parallel if specified
    if GPUS > 1:

        #use multigpu model checkpoint
        ckp_saver = ModelCheckpointMultiGPU(checkpoint_dir + "/model.{epoch:02d}-{loss:.2f}.hdf5", monitor='loss', verbose=0,
                                    save_best_only=False,
                                    save_weights_only=True, mode='auto', period=1)

        cb.append(ckp_saver)
        print("Using multi gpu support with {} GPUs".format(GPUS))

        # make the model parallel
        parallel_model = multi_gpu_model(squeeze.model, gpus=GPUS)
        parallel_model.compile(optimizer=opt,
                              loss=[squeeze.loss], metrics=[squeeze.loss_without_regularization, squeeze.bbox_loss, squeeze.class_loss, squeeze.conf_loss])



        #actually do the training
        parallel_model.fit_generator(train_generator, epochs=EPOCHS,
                                        steps_per_epoch=nbatches_train, callbacks=cb)


    else:

        # add a checkpoint saver
        ckp_saver = ModelCheckpoint(checkpoint_dir + "/model.{epoch:02d}-{loss:.2f}.hdf5", monitor='loss', verbose=1,
                                    save_best_only=False,
                                    save_weights_only=True, mode='auto', period=1)
        cb.append(ckp_saver)


        print("Using single GPU")
        #compile model from squeeze object, loss is not a function of model directly
        squeeze.model.compile(optimizer=opt,
                              loss=[squeeze.loss], metrics=[squeeze.loss_without_regularization, squeeze.bbox_loss, squeeze.class_loss, squeeze.conf_loss])

        #actually do the training
        squeeze.model.fit_generator(train_generator, epochs=EPOCHS,
                                        steps_per_epoch=nbatches_train, callbacks=cb)


    # gc.collect()


if __name__ == "__main__":



    #parse arguments
    parser = argparse.ArgumentParser(description='Train squeezeDet model.')
    parser.add_argument("--steps",  type=int, help="steps per epoch. DEFAULT: #imgs/ batch size")
    parser.add_argument("--epochs", type=int, help="number of epochs. DEFAULT: 100")
    parser.add_argument("--optimizer",  help="Which optimizer to use. DEFAULT: SGD with Momentum and lr decay OPTIONS: SGD, ADAM")
    parser.add_argument("--logdir", help="dir with checkpoints and loggings. DEFAULT: ./log")
    parser.add_argument("--img", help="file of full path names for the training images. DEFAULT: img_train.txt")
    parser.add_argument("--gt", help="file of full path names for the corresponding training gts. DEFAULT: gt_train.txt")
    parser.add_argument("--gpu",  help="which gpu to use. DEFAULT: 0")
    parser.add_argument("--gpus", type=int,  help="number of GPUS to use when using multi gpu support. Overwrites gpu flag. DEFAULT: 1")
    parser.add_argument("--init",  help="keras checkpoint to start training from. If argument is none, training starts from the beginnin. DEFAULT: init_weights.h5")
    parser.add_argument("--resume", type=bool, help="Resumes training and does not delete old dirs. DEFAULT: False")
    parser.add_argument("--reducelr", type=bool, help="Add ReduceLrOnPlateu callback to training. DEFAULT: True")
    parser.add_argument("--verbose", type=bool,  help="Prints additional information. DEFAULT: False")
    parser.add_argument("--config",   help="Dictionary of all the hyperparameters. DEFAULT: squeeze.config")

    args = parser.parse_args()


    #set global variables
    if args.img is not None:
        img_file = args.img
    if args.gt is not None:
        gt_file = args.gt
    if args.logdir is not None:
        log_dir_name = args.logdir
    if args.gpu is not None:
        CUDA_VISIBLE_DEVICES = args.gpu
    if args.epochs is not None:
        EPOCHS = args.epochs
    if args.steps is not None:
        STEPS = args.steps
    if args.optimizer is not None:
        OPTIMIZER = 'args.optimizer.lower()'
    if args.init is not None:
        init_file = args.init
    if args.gpus is not None:
        GPUS= args.gpus
    if args.reducelr is not None:
        REDUCELRONPLATEAU = args.reducelr
    if args.verbose is not None:
        VERBOSE=args.verbose
    if args.config is not None:
        CONFIG = args.config

    train()
